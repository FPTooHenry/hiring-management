package com.fu.swp391.common.enumConstants;

public class roleEnum {
    public static final String ADMIN = "ADMIN";
    public static final String USER = "USER";
    public static final String CANDIDATE = "CANDIDATE";
    public static final String GUEST = "GUEST";
    public static final String COMPANY = "COMPANY";
}
