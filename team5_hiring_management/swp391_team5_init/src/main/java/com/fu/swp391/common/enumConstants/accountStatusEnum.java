package com.fu.swp391.common.enumConstants;

public class accountStatusEnum {
    public static final String USER_CREATED = "CREATED";
    public static final String USER_ACTIVATED = "ACTIVE";
    public static final String USER_INACTIVATED = "INACTIVE";
}
